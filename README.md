
# 30V AC to 24V DC converter

This is a simple circuitary which is convert the 30V AC to 24V DC regulated supply.
"EASY EDA" is used for designing the schematic and PCB.


### Circuit diagram 

![schematic!](schematic.PNG)

### PCB

![converter1D!](converter1D.PNG)

### 2D view

![converter2D!](converter2D.PNG)

### 3D view

![converter3D!](converter3D.PNG)